// pop-up de message d'information
const infoMessage = (message) => {
    Swal.fire({
    icon: 'info',
    text: message,
    confirmButtonColor: '#446372',
    })
}

// pop-up de message de succes
const successMessage = (message) => {
    Swal.fire({
    icon: 'success',
    text: message,
    confirmButtonColor: '#446372',
    })
}

// pop-up de message d'avertissement
const warningMessage = (message) => {
    Swal.fire({
    icon: 'warning',
    text: message,
    confirmButtonColor: '#446372',
    })
}

// pop-up de message d'erreur
const errorMessage = (message) => {
    Swal.fire({
    icon: 'error',
    text: message,
    confirmButtonColor: '#446372',
    })
}

// pop-up de confirmation d'action
/*const confirmAction = () => {
    return Swal.fire({
        icon: "question",
        text: 'Voulez-vous supprimer ce collaborateur ?',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Oui',
        cancelButtonText: `Annuler`,
    }).then((result) => {
        return result.isConfirmed
    })

}*/