document.addEventListener("DOMContentLoaded", () => {
    const userName = $('#username')
    const password = $('#password')
    const passwordBis = $('#password-bis')
    const form = $('#sign-form')

    const formValidate = () => {
        if(userName.val().length < 4){
            $('.username-group .error-message').text("4 caractères minimum").addClass("d-block")
            return false
        }else{
            $('.username-group .error-message').removeClass("d-block")
        }
        if(password.val().length < 6){
            $('.password-group .error-message').text("6 caractères minimum").addClass("d-block")
            return false
        }else{
            $('.password-group .error-message').removeClass("d-block")
        }
        if(passwordBis.val().length < 6){
            $('.password-bis-group .error-message').text("6 caractères minimum").addClass("d-block")
            return false
        }else{
            if(passwordBis.val() !== password.val()){
                $('.password-bis-group .error-message').text("Les deux mots de passe ne correspondent pas").addClass("d-block")
                return false
            }
            $('.password-bis-group .error-message').removeClass("d-block")
        }
        return true;
    }
    form.on("submit" , async (e) => {
        e.preventDefault()
        if(formValidate()){
            try {
                const response = await signUp(userName.val(), password.val())
                if(response.result.status === "done"){
                    // set du token
                    const tokenObj = {
                        id: response.result.id,
                        token: response.result.token
                    }
                    setToken(tokenObj)
                    setUserName(userName.val())
                    // redirection page d'accueil
                    location.replace("../index.html")
                }else{
                    infoMessage("Nom d'utilisateur indisponible")
                }
            } catch (error) {
                console.error(error);
            }
        }
    })
})