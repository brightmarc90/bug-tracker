const BASE_URL = "http://greenvelvet.alwaysdata.net/bugTracker/api"

// fonction d'inscription à l'appli
const signUp = async (user, password) => {
    return await fetch(`${BASE_URL}/signup/${user}/${password}`)
        .then(response => response.json())
}

// fonction d'authentification à l' appli
const login = async (user, password) => {
    return await fetch(`${BASE_URL}/login/${user}/${password}`)
        .then(response => response.json())
}

// fonction de récup de tous les bugs
const getAllBugs = async (token) => {
    return await fetch(`${BASE_URL}/list/${token}/0`)
        .then(response => response.json())
}

// fonction de récup de tous les developpeurs
const getAllDevs = async (token) => {
    return await fetch(`${BASE_URL}/users/${token}`)
        .then(response => response.json())
}

// fonction de récup de tous les bugs
const getDevBugs = async (token, userId) => {
    return await fetch(`${BASE_URL}/list/${token}/${userId}`)
        .then(response => response.json())
}

// fonction d'ajout d'un bug
const addOneBug = async (token, userId, title, description) => {
    return await fetch(`${BASE_URL}/add/${token}/${userId}`, {
        method: "POST",
        body: JSON.stringify({
            title: title,
            description: description
        })
    })
    .then(response => response.json())
}

// fonction de modification de l'état d'un bug
const updateOneBug = async (token, bugId, newState) => {
    return await fetch(`${BASE_URL}/state/${token}/${bugId}/${newState}`)
        .then(response => response.json())
}

// fonction de suppresion d'un bug
const deleteOneBug = async (token, bugId) => {
    return await fetch(`${BASE_URL}/delete/${token}/${bugId}`)
        .then(response => response.json())
}