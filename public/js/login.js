document.addEventListener("DOMContentLoaded", () => {
    // verification de la validité des champs
    const userName = $('#username')
    const password = $('#password')
    const form = $('#login-form')

    const formValidate = () => {
        if(userName.val().length === 0){
            $('.username-group .error-message').text("Ce champ ne peut être vide").addClass("d-block")
            return false
        }else{
            $('.username-group .error-message').removeClass("d-block")
        }
        if(password.val().length === 0){
            $('.password-group .error-message').text("Ce champ ne peut être vide").addClass("d-block")
            return false
        }else{
            $('.password-group .error-message').removeClass("d-block")
        }
        return true
    }
    // clic sur le bouton valider
    form.on("submit", async (e) => {
        e.preventDefault();
        if(formValidate()){
            // connexion à l'appli
            try {
                const response = await login(userName.val(), password.val());
                if(response.result.status === "done"){
                    // set du token
                    const tokenObj = {
                        id: response.result.id,
                        token: response.result.token
                    }
                    setToken(tokenObj)
                    setUserName(userName.val())
                    // redirection page d'accueil
                    location.replace("../index.html")
                }else{
                    infoMessage("Login ou mot de passe incorrects")
                }
            } catch (error) {
                console.error(error);
            }
        }
    })
})