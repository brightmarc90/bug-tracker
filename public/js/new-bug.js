document.addEventListener("DOMContentLoaded", () => {
    // redirection si pas connecté
    if(!isTokenSet()){
        location.replace("./login.html")
    }
    // récup du nom d'utilisateur
    $("#username").html(`<span class="material-icons">account_circle</span> ${getUserName()}`);
    // clic sur bouton deconnexion
    $(".logout").on("click", () => {
        removeToken()
        location.replace("./login.html")
    })
    // récupération des valeurs et validation formulaire
    const title = $('#bug-title')
    const description = $('#bug-desc')
    const form = $('#add-bug-form')

    const formValidate = () => {
        if(title.val().length === 0){
            $('.bug-title-group .error-message').text("Ce champ ne peut être vide").addClass("d-block")
            return false
        }else{
            $('.bug-title-group .error-message').removeClass("d-block")
        }
        if(description.val().length === 0){
            $('.bug-desc-group .error-message').text("Ce champ ne peut être vide").addClass("d-block")
            return false
        }else{
            $('.bug-desc-group .error-message').removeClass("d-block")
        }
        return true
    }

    // clic sur le bouton valider
    form.on("submit", async (e) => {
        e.preventDefault();
        if(formValidate()){
            // enregistrement du bug
            try {
                const {token, id} = getToken()
                const response = await addOneBug(token, id, title.val(), description.val())
                if(response.result.status === "done"){
                    successMessage("Bug enregistré")
                    // redirection
                    setTimeout(() => location.replace("../index.html"), 3000) 
                }else{
                    errorMessage("Une erreur s'est produite pendant l'enregistrement")
                }
            } catch (error) {
                console.error(error);
            }
        }
    })
})