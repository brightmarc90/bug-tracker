document.addEventListener("DOMContentLoaded", () => {
    // redirection si pas connecté
    if(!isTokenSet()){
        location.replace("./login.html")
    }
    // récup du nom d'utilisateur
    $("#username").html(`<span class="material-icons">account_circle</span> ${getUserName()}`);
    // clic sur bouton deconnexion
    $(".logout").on("click", () => {
        removeToken()
        location.replace("./login.html")
    })
    const selectChange = async (e, token, bugId) => {
        try {
            const response = await updateOneBug(token, bugId, e.target.value)
            if(response.result.status === "done"){
                successMessage("Etat du bug mis à jour")
                setTimeout(() => location.replace("./todo.html"), 3000) 
            }else{
                errorMessage("Une erreur est survenue pendant la mise à jour du bug")
            }
        } catch (error) {
            console.error(error);
        }
    }
    // clic sur bouton supprimer
    const btnDelClick = (e, token, bugId) => {
        const execAsync = async () => {
            try {
                const response = await deleteOneBug(token, bugId)
                if(response.result.status === "done"){
                    successMessage("Bug supprimé")
                    setTimeout(() => location.replace("./todo.html"), 3000)
                }else{
                    errorMessage("Une erreur est survenue pendant la suppression du bug")
                }
            } catch (error) {
                console.error(error);
            }
        }
        Swal.fire({
            icon: "question",
            text: 'Voulez-vous supprimer ce collaborateur ?',
            showCancelButton: true,
            confirmButtonColor: '#446372',
            confirmButtonText: 'Oui',
            cancelButtonText: `Annuler`,
        }).then((result) => {
            if (result.isConfirmed) {
                execAsync()
            }
        })
    }
    // exécution de la requête et affichage de la liste des bugs
    try {
        const execAsync = async () => {
            const {token, id} = getToken()
            const bugResponse = await getDevBugs(token, id)
            const bugs = bugResponse.result.bug
            console.log(bugs);
            // mise à jour des stats
            $(".bugs-stats .total .number").text(bugs.length)
            $(".bugs-stats .progres .number").text(bugs.filter(bug => bug.state === "1").length)
            $(".bugs-stats .treated .number").text(bugs.filter(bug => bug.state === "2").length)
            // remplissage du tableau
            if(bugs.length > 0){
                bugs.forEach((bug, index) => {
                    $("#bugs-table tbody").append(`
                        <tr id="tr-${bug.id}">
                            <td>${index+1}</td>
                            <td title="${bug.description}">${bug.title}</td>
                            <td>${new Date(bug.timestamp*1000).toLocaleDateString("fr-FR")}</td>
                            <td>
                                <select name="bug-state" class="bug-state">
                                    <option value="0">Non traité</option>
                                    <option value="1">En cours</option>
                                    <option value="2">Traité</option>
                                </select>
                            </td>
                            <td><button class="btn-del" title="Supprimer"><span class="material-icons">delete</span></button></td>
                        </tr>
                    `)
                    $(`#tr-${bug.id} select`).val(bug.state)
                    $(`#tr-${bug.id} select`).on("change", (e) => selectChange(e, token, bug.id))
                    $(`#tr-${bug.id} .btn-del`).on("click", (e) => btnDelClick(e, token, bug.id))
                });
            }else{
                $("#bugs-table").addClass("d-none")
                $(".no-bug").addClass("d-block")
            }   
        }
        execAsync()
    } catch (error) {
        console.log(error);
    }
})